﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simulation
{
    abstract class State
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Transitions { get; set; }

        public abstract string DoTransition(Player p, int id);
    }
}
