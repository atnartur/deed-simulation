﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simulation.States
{
    class ModifyAPI:State
    {
        public ModifyAPI()
        {
            Name = "Нужно доработать API";
            Description = "Вы понимаете, что API вашего проекта не работает как надо из-за огромного количества ошибок.";
            Transitions = new List<string>() { "Доработать самостоятельно", "Ждать, пока команда исправит API" };
        }

        public override string DoTransition(Player p, int id)
        {
            if (id == 0)
            {
                p.Rest -= 2;
                return "Вы тратите своё время на доработку API. Люди начинают обращать внимание на ваши мешки под глазами";
            }
            else
            {
                p.Time -= 2;
                p.Rest++;
                return "Вы ждёте, пока команда исправит API. На это уходит время, но вы можете отдохнуть";
            }
        }
    }
}
