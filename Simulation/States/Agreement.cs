﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simulation.States
{
    class Agreement:State
    {
        public Agreement()
        {
            Name = "Согласование";
            Description = "Настало время отправиться на согласование проекта с заказчиком.";
            Transitions = new List<string>() { "Отправиться на согласование" };
        }

        public override string DoTransition(Player p, int id)
        {
            Random r = new Random();
            if (r.Next(10) > p.Sureness * 10)
            {
                p.Time--;
                return "Заказчик остался недоволен вашим пониманием требований. Вы теряете время на доработку.";
            }
            else
            {
                return "Вы согласовали требования с заказчиком и готовы приступать к разработке.";
            }
        }
    }
}
