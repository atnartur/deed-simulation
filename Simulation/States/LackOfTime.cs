﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simulation.States
{
    class LackOfTime:State
    {
        public LackOfTime()
        {
            Name = "Время подходит к концу.";
            Description = "Скоро дедлайн. Вы понимаете, что, такими темпами, проект не будет готов к релизу.";
            Transitions = new List<string>() { "Ускориться.", "Упростить.", "Искать нового разработчика." };
        }

        public override string DoTransition(Player p, int id)
        {
            if (id == 0)
            {
                p.Rest -= 2;
                p.Progress++;
                return "Вам необходимо ускориться. Вы включаетесь в разработку и забываете про сон.";
            }
            else if (id == 1)
            {
                p.Progress--;
                return "Вы решаете упростить проект. Приходится отказываться от некоторых фич.";
            }
            else
            {
                p.Rest--;
                p.Money--;
                p.Progress++;
                return "Вы ищете нового разработчика. На это уходят время и деньги, но шансы закончить проект вовремя возрастают.";
            }
        }
    }
}
