﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simulation.States
{
    class Development:State
    {
        public Development()
        {
            Name = "Разработка";
            Description = "Вы, наконец-то, можете приступать к разработке";
            Transitions = new List<string>() { "Начать разработку" };
        }

        public override string DoTransition(Player p, int id)
        {
            return "Разработка идёт своим ходом. Команда работает.";
        }
    }
}
