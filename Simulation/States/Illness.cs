﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simulation.States
{
    class Illness:State
    {
        public Illness()
        {
            Name = "Заболел разработчик";
            Description = "Один из ваших разработчиков позвонил вам, чтобы сказать, что он не может работать из-за болезни. Что будете делать?";
            Transitions = new List<string>() { "Искать нового разработчика.", "Оставить как есть, команда справится.", "Перепланировать задачи." };
        }

        public override string DoTransition(Player p, int id)
        {
            if (id == 0)
            {
                p.Time--;
                p.Money--;
                p.Arch--;
                return "Вы теряете время и деньги на найм нового разработчика. Ему ещё нужно разобраться в архитектуре проекта";
            }
            else if (id == 1)
            {
                p.Arch -= 2;
                return "Вы решаете оставить всё как есть. Команде приходится выполнять больше задач, из-за этого страдает архитектура проекта.";
            }
            else
            {
                p.Rest--;
                return "Перепланировка требует времени. Вы засиживаетесь допоздна и не высыпаетесь.";
            }
        }
    }
}
