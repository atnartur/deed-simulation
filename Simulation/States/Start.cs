﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simulation.States
{
    class Start:State
    {
        public Start()
        {
            Name = "Недописано ТЗ";
            Description = "Вы обнаруживаете, что ваше ТЗ недописано.";
            Transitions = new List<string>() { "Дописать ТЗ.", "Оставить как есть." };
        }

        public override string DoTransition(Player p, int id)
        {
            if (id == 0)
            {
                p.Time--;
                p.Sureness += 0.5;
                return "Вы решили дописать ТЗ. Вы теряете на это время, но становитесь более уверенными в успехе проекта.";
            }
            else
            {
                p.Arch--;
                p.Sureness -= 0.2;
                return "Вы решили оставить ТЗ как есть. Вы менее уверены в успехе проекта.";
            }
        }
    }
}
