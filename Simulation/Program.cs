﻿using System;
using Simulation.States;

namespace Simulation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Вы - менеджер проекта. Вам пришёл новый заказ. Постарайтесь в этот раз довести всё до конца.");

            Player p = new Player
            {
                Time = 5,
                Sureness = 0.4,
                Arch = 3,
                Rest = 3,
                Progress = 0,
                Money = 5,
                ProjectState = PlayerState.Development,
                StateId = 0
            };

            State[] states = new State[] 
            {
                new Start(),
                new Agreement(),
                new Development(),
                new Illness(),
                new ModifyAPI(),
                new LackOfTime()
            };

            while (p.ProjectState != PlayerState.Release)
            {
                if (p.ProjectState == PlayerState.Development)
                {
                    Console.WriteLine($"[Состояние игры] {p}");
                    Console.WriteLine($"---- {states[p.StateId].Name} ----");
                    Console.WriteLine(states[p.StateId].Description);
                    Console.WriteLine("Ваши действия?");
                    for (int i = 0; i < states[p.StateId].Transitions.Count; i++)
                    {
                        Console.WriteLine($"{i + 1}. {states[p.StateId].Transitions[i]}");
                    }
                    int t = int.Parse(Console.ReadLine().ToString()) - 1;
                    Console.WriteLine(states[p.StateId].DoTransition(p, t));
                    if (p.StateId >= 3 && p.StateId <= 5)
                    {
                        p.Progress++;
                    }
                    p.StateId++;
                    if (p.Time <= 0 || p.StateId == 6)
                    {
                        p.ProjectState = PlayerState.Release;
                    }
                    if (p.Rest <= 0)
                    {
                        p.ProjectState = PlayerState.Sleep;
                    }
                }
                else if (p.ProjectState == PlayerState.Sleep)
                {
                    p.Rest += 2;
                    p.Time -= 3;
                    Console.WriteLine("Вы отрубились из-за усталости. Проснувшись, вы чувствуете себя отдохнувшим, но понимаете, что время потеряно безвозвратно.");
                    if (p.Time > 0)
                    {
                        p.ProjectState = PlayerState.Development;
                    }
                    else
                    {
                        p.ProjectState = PlayerState.Release;
                    }
                }

                Console.WriteLine();
            }

            if (p.Progress >= 3)
            {
                Console.WriteLine("Вы довели проект до конца! Хорошая работа!");
            }
            else
            {
                Console.WriteLine("Вы не смогли довести проект до конца. Заказчик недоволен.");
            }
            
            Console.WriteLine($"[Финальное состояние игры] {p}");
        }
    }
}
