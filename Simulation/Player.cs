﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simulation
{
    enum PlayerState { Development, Sleep, Release }
    class Player
    {
        public int Time { get; set; }
        public double Sureness { get; set; }
        public int Arch { get; set; }
        public int Rest { get; set; }
        public int Money { get; set; }
        public int Progress { get; set; }
        public int StateId { get; set; }
        public PlayerState ProjectState { get; set; }

        public override string ToString()
        {
            return $"время - {Time}, прогресс - {Progress}, уверенность - {Sureness}, " +
                   $"архитектура - {Arch}, отдых - {Rest}, деньги - {Money}, состояние проекта - {ProjectState}";
        }
    }
}
